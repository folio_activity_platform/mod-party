{
  "permissionName": "partyNotifyData.collection.get",
  "displayName": "活动通知数据模块 - 获取活动通知数据列表",
  "description": "获取一组活动通知数据列表"
},
{
"permissionName": "partyNotifyData.item.get",
"displayName": "活动通知数据模块 - 获取一条活动通知数据",
"description": "获取一条活动通知数据"
},
{
"permissionName": "partyNotifyData.item.post",
"displayName": "活动通知数据模块 - 创建一个活动通知数据",
"description": "创建一个活动通知数据"
},
{
"permissionName": "partyNotifyData.item.put",
"displayName": "活动通知数据模块 - 修改一条活动通知数据",
"description": "修改一条活动通知数据"
},
{
"permissionName": "partyNotifyData.item.delete",
"displayName": "活动通知数据模块 - 删除一条活动通知数据",
"description": "删除一条活动通知数据"
},