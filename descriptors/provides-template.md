# 配置权限模版 使用方式搜索替换相对应的名字
{
  "id": "readerAttendData",
  "version": "1.0",
  "handlers": [
    {
      "methods": [
        "GET"
      ],
      "pathPattern": "/readerAttendData",
      "permissionsRequired": [
        "readerAttendData.collection.get"
      ]
    },
    {
      "methods": [
        "POST"
      ],
      "pathPattern": "/readerAttendData",
      "permissionsRequired": [
        "readerAttendData.item.post"
      ],
      "modulePermissions": [
      ]
    },
    {
      "methods": [
        "POST"
      ],
      "pathPattern": "/readerAttendData/{uid}",
      "permissionsRequired": [
        "readerAttendData.item.post"
      ]
    },
    {
      "methods": [
        "GET"
      ],
      "pathPattern": "/readerAttendData/{id}",
      "permissionsRequired": [
        "readerAttendData.item.get"
      ]
    },
    {
      "methods": [
        "PUT"
      ],
      "pathPattern": "/readerAttendData/{id}",
      "permissionsRequired": [
        "readerAttendData.item.put"
      ]
    },
    {
      "methods": [
        "DELETE"
      ],
      "pathPattern": "/readerAttendData/{id}",
      "permissionsRequired": [
        "readerAttendData.item.delete"
      ]
    }
  ]
}