package org.folio.rest.impl.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

/**
 * @author lee
 * @Classname NotifyExcel
 * @Description TODO
 * @Date 2020/7/2 14:54
 * @Created by lee
 */
public class NotifyLogExcel {
    /**
     * name
     *
     */
    @JsonProperty("name")
    @JsonPropertyDescription("name")
    private String name;
    /**
     * message
     *
     */
    @JsonProperty("message")
    @JsonPropertyDescription("message")
    private String message;
    /**
     * sendDate
     *
     */
    @JsonProperty("sendDate")
    @JsonPropertyDescription("sendDate")
    private String sendDate;
    /**
     * notifyMode
     *
     */
    @JsonProperty("notifyMode")
    @JsonPropertyDescription("notifyMode")
    private String notifyMode;
    /**
     * accountType
     *
     */
    @JsonProperty("accountType")
    @JsonPropertyDescription("accountType")
    private String accountType;
    /**
     * messageType
     *
     */
    @JsonProperty("messageType")
    @JsonPropertyDescription("messageType")
    private String messageType;
    /**
     * phoneNumber
     *
     */
    @JsonProperty("phoneNumber")
    @JsonPropertyDescription("phoneNumber")
    private String phoneNumber;
    /**
     * userAccount
     *
     */
    @JsonProperty("userAccount")
    @JsonPropertyDescription("userAccount")
    private String userAccount;
    /**
     * partyName
     *
     */
    @JsonProperty("partyName")
    @JsonPropertyDescription("partyName")
    private String partyName;
    /**
     * reserve Channel
     *
     */
    @JsonProperty("reserveChannel")
    @JsonPropertyDescription("reserve Channel")
    private String reserveChannel;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSendDate() {
        return sendDate;
    }

    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }

    public String getNotifyMode() {
        return notifyMode;
    }

    public void setNotifyMode(String notifyMode) {
        this.notifyMode = notifyMode;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public String getReserveChannel() {
        return reserveChannel;
    }

    public void setReserveChannel(String reserveChannel) {
        this.reserveChannel = reserveChannel;
    }
}
