package org.folio.rest.impl.pojo;

/**
 * @author lee
 * @Classname AttendExportObject
 * @Description TODO
 * @Date 2020/7/22 16:58
 * @Created by lee
 */
public class AttendExportObject {
    private String partyname;
    private String readername;
    private String state;
    private String attendstate;
    private String attenddate;
    private String partystartdate;

    public String getPartyname() {
        return partyname;
    }

    public void setPartyname(String partyname) {
        this.partyname = partyname;
    }

    public String getReadername() {
        return readername;
    }

    public void setReadername(String readername) {
        this.readername = readername;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAttendstate() {
        return attendstate;
    }

    public void setAttendstate(String attendstate) {
        this.attendstate = attendstate;
    }

    public String getAttenddate() {
        return attenddate;
    }

    public void setAttenddate(String attenddate) {
        this.attenddate = attenddate;
    }

    public String getPartystartdate() {
        return partystartdate;
    }

    public void setPartystartdate(String partystartdate) {
        this.partystartdate = partystartdate;
    }
}
