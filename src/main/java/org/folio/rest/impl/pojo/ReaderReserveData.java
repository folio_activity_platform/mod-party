package org.folio.rest.impl.pojo;

/**
 * @author lee
 * @Classname ReaderReserveData
 * @Description TODO
 * @Date 2020/7/7 23:22
 * @Created by lee
 */
public class ReaderReserveData {
    private String reserveId;
    private String partyName;
    private String partyVenue;
    private String partyStartDate;
    private String partyEndDate;
    private String imageId;
    private Integer attendState;

    public String getReserveId() {
        return reserveId;
    }

    public void setReserveId(String reserveId) {
        this.reserveId = reserveId;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public String getPartyVenue() {
        return partyVenue;
    }

    public void setPartyVenue(String partyVenue) {
        this.partyVenue = partyVenue;
    }

    public String getPartyStartDate() {
        return partyStartDate;
    }

    public void setPartyStartDate(String partyStartDate) {
        this.partyStartDate = partyStartDate;
    }

    public String getPartyEndDate() {
        return partyEndDate;
    }

    public void setPartyEndDate(String partyEndDate) {
        this.partyEndDate = partyEndDate;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public Integer getAttendState() {
        return attendState;
    }

    public void setAttendState(Integer attendState) {
        this.attendState = attendState;
    }
}
