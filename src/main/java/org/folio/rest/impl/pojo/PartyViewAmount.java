package org.folio.rest.impl.pojo;

/**
 * @author lee
 * @Classname PartyViewAmount
 * @Description TODO
 * @Date 2020/7/21 17:18
 * @Created by lee
 */
public class PartyViewAmount {

    private String id;
    private String partyId;
    private Integer viewAmount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    public Integer getViewAmount() {
        return viewAmount;
    }

    public void setViewAmount(Integer viewAmount) {
        this.viewAmount = viewAmount;
    }
}
