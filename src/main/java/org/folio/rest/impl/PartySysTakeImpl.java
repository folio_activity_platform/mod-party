package org.folio.rest.impl;

import io.vertx.core.AsyncResult;
import io.vertx.core.Context;
import io.vertx.core.Handler;
import org.folio.rest.impl.other.take.TenantsTaskService;
import org.folio.rest.jaxrs.resource.PartySysTake;

import javax.ws.rs.core.Response;
import java.util.Map;

import static io.vertx.core.Future.succeededFuture;

public class PartySysTakeImpl implements PartySysTake {

    @Override
    public void postPartySysTake(String entity, Map<String, String> okapiHeaders, Handler<AsyncResult<Response>> asyncResultHandler, Context vertxContext) {
        TenantsTaskService.startTake(vertxContext,okapiHeaders);

        asyncResultHandler.handle(succeededFuture(PostPartySysTakeResponse.respond201WithApplicationJson(entity,PostPartySysTakeResponse.headersFor201())));

    }
}
