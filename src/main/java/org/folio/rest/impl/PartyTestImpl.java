package org.folio.rest.impl;

import io.vertx.core.AsyncResult;
import io.vertx.core.Context;
import io.vertx.core.Handler;
import org.folio.rest.jaxrs.resource.PartyTest;

import javax.ws.rs.core.Response;
import java.util.Map;

import static io.vertx.core.Future.succeededFuture;

public class PartyTestImpl implements PartyTest {
    @Override
    public void postPartyTest(String entity, Map<String, String> okapiHeaders, Handler<AsyncResult<Response>> asyncResultHandler, Context vertxContext) {
          asyncResultHandler.handle(succeededFuture(PostPartyTestResponse.respond201WithApplicationJson("success",PostPartyTestResponse.headersFor201())));
    }
}
