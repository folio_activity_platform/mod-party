package org.folio.rest.impl.util;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import org.folio.rest.tools.utils.BinaryOutStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

/**
 * @author lee
 * @Classname FileExport
 * @Description TODO
 * @Date 2020/7/2 09:37
 * @Created by lee
 */
public class FileExport {


    public BinaryOutStream fileExport(Map<String, String> headerAlias, Iterable<?> data) {
        BinaryOutStream binaryOutStream = new BinaryOutStream();
        String fileName = DateUtil.format(new Date(), DatePattern.PURE_DATETIME_MS_FORMAT) + RandomUtil.randomNumbers(4);
        ExcelWriter writer = ExcelUtil.getWriter("/tmp/" + fileName + ".xls");
        writer.write(data);
        if (headerAlias != null && headerAlias.size() > 0) {
            writer.setHeaderAlias(headerAlias);
        }
        writer.close();
        File file = new File("/tmp/" + fileName + ".xls");
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(file);
            byte[] b = new byte[fileInputStream.available()];
            fileInputStream.read(b);
            binaryOutStream.setData(b);

            return binaryOutStream;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                FileUtil.del(file);
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
        return null;

    }

    public static void fileExports(Map<String, String> headerAlias, Iterable<?> data, Handler<AsyncResult<BinaryOutStream>> handler) {
        Future<BinaryOutStream> future = Future.future();
        FileInputStream fileInputStream = null;
        File file = null;
        BinaryOutStream binaryOutStream = null;
        try {
            binaryOutStream = new BinaryOutStream();
            String fileName = DateUtil.format(new Date(), DatePattern.PURE_DATETIME_MS_FORMAT) + RandomUtil.randomNumbers(4);
            ExcelWriter writer = ExcelUtil.getWriter("/tmp/" + fileName + ".xls");
            if (headerAlias != null && headerAlias.size() > 0) {
                writer.setHeaderAlias(headerAlias);
            }
            writer.write(data);
            writer.close();
            file = new File("/tmp/" + fileName + ".xls");
            fileInputStream = new FileInputStream(file);
            byte[] b = new byte[fileInputStream.available()];
            fileInputStream.read(b);
            binaryOutStream.setData(b);

        } catch (FileNotFoundException e) {
            Future.failedFuture(e);
        } catch (IOException e) {
            Future.failedFuture(e);
        } finally {
            if (fileInputStream != null) {

                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            if (file != null) {
                FileUtil.del(file);
            }
        }

        future.setHandler(r -> {
              if (r.succeeded()){
                  handler.handle(Future.succeededFuture(r.result()));
              }else {
                  handler.handle(Future.failedFuture(r.cause()));
              }
        });
        future.complete(binaryOutStream);
    }
}
