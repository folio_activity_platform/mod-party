package org.folio.rest.impl.util;

public class DBName {
    
    public static final String NOTIFY_TABLE_ATTEND = "attend";

    public static final String NOTIFY_TABLE_AUXILIARY = "auxiliary";

    public static final String NOTIFY_TABLE_NOTIFY_LOG = "notify_log";

    public static final String NOTIFY_TABLE_NOTIFY_TEMPLATE = "notify_template";

    public static final String NOTIFY_TABLE_PARTY_APPRAISAL = "party_appraisal";

    public static final String NOTIFY_TABLE_PARTY = "party";

    public static final String NOTIFY_TABLE_PARTY_LOG = "party_log";
    
    public static final String NOTIFY_TABLE_READER_CREDIT = "reader_credit";

    public static final String NOTIFY_TABLE_READER_CREDIT_RELATED = "reader_credit_related";

    public static final String NOTIFY_TABLE_PARTY_RULES = "party_rules";

    public static final String NOTIFY_TABLE_PARTY_SET = "party_set";

    public static final String NOTIFY_TABLE_PARTY_IMAGE = "party_image";

    public static final String NOTIFY_TABLE_PROPERTY = "property";

    public static final String NOTIFY_TABLE_RESERVE = "reserve";

    public static final String NOTIFY_TABLE_PARTY_NOTIFY_DATA = "party_notify_data";

    public static final String NOTIFY_TABLE_PARTY_VIEW_AMOUNT = "party_view_amount";
}
