package org.folio.rest.impl;

import cn.hutool.cron.CronUtil;
import io.vertx.core.*;

import org.folio.rest.impl.other.blacklist.BlacklistService;
import org.folio.rest.impl.other.delayjob.partyattend.AttendDelayThreadInit;
import org.folio.rest.impl.other.delayjob.partydelay.DelayThreadHeader;
import org.folio.rest.impl.other.reserve_station.ReserveStation;
import org.folio.rest.impl.pojo.WebPartyViews;
import org.folio.rest.resource.interfaces.InitAPI;

public class InitApiImpl implements InitAPI {

    @Override
    public void init(Vertx vertx, Context context, Handler<AsyncResult<Boolean>> handler) {
        try{
//            new DelayThreadHeader().initThread(vertx);
//            new AttendDelayThreadInit().initThread(vertx);
//            new WebPartyViews(vertx);
//            new ReserveStation(vertx);
//            new BlacklistService(vertx);
            CronUtil.start();
            CronUtil.setMatchSecond(true);
        }catch (Exception e){
            e.printStackTrace();
        }

        handler.handle(Future.succeededFuture(true));
    }
}
