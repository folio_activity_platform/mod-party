package org.folio.rest.impl;

import cn.hutool.cron.CronUtil;
import io.vertx.core.*;
import org.folio.rest.impl.other.blacklist.BlacklistService;
import org.folio.rest.impl.other.delayjob.partyattend.AttendDelayThreadInit;
import org.folio.rest.impl.other.delayjob.partydelay.DelayThreadHeader;
import org.folio.rest.impl.other.reserve_station.ReserveStation;
import org.folio.rest.impl.pojo.WebPartyViews;
import org.folio.rest.resource.interfaces.PostDeployVerticle;

public class InitConfigService implements PostDeployVerticle {
    @Override
    public void init(Vertx vertx, Context context, Handler<AsyncResult<Boolean>> handler) {

        handler.handle(Future.succeededFuture(true));
    }
}
