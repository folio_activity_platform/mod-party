package org.folio.rest.impl.other.blacklist;

public class BlackListRecordRelated {

    private String id;//等同 attendId

    private String attendId;//对应产生的签到记录的编号

    private String readerRecordId;//对应的行为记录表编号

    private String barcode;//读者证号

    private String attendStatus;//签到状态

    private String createDate;//创建的时间

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAttendId() {
        return attendId;
    }

    public void setAttendId(String attendId) {
        this.attendId = attendId;
    }

    public String getReaderRecordId() {
        return readerRecordId;
    }

    public void setReaderRecordId(String readerRecordId) {
        this.readerRecordId = readerRecordId;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getAttendStatus() {
        return attendStatus;
    }

    public void setAttendStatus(String attendStatus) {
        this.attendStatus = attendStatus;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
}
