package org.folio.rest.impl.other.delayjob.partyattend;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import io.vertx.core.Vertx;

/**
 * @author lee
 */
public class AttendDelayThreadInit {
   private static ThreadPoolExecutor threadPool = new ThreadPoolExecutor(
            1,1,10, TimeUnit.SECONDS,
            new ArrayBlockingQueue<Runnable>(1),
            new ThreadPoolExecutor.DiscardOldestPolicy());
   public static PartyAttendHandler initThread(Vertx vertx,String tenant )   {
     PartyAttendHandler attendDelayThread = new PartyAttendHandler(vertx,tenant);
	 //attendDelayThread.initData();
     threadPool.execute(attendDelayThread);
     return attendDelayThread;
   }
}
