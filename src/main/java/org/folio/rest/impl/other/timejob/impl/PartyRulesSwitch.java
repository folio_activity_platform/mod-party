package org.folio.rest.impl.other.timejob.impl;

import org.folio.rest.impl.other.take.TenantsTaskService;
import org.folio.rest.impl.other.timejob.TimeJob;

public class PartyRulesSwitch extends TimeJob {
    @Override
    public void job(){
        TenantsTaskService.timeTakeBySwitchRule();
        super.show("更新年度黑名单规则扫描开始。。。");
    }
}
