package org.folio.rest.impl.other.take;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import org.folio.rest.impl.other.reserve_station.ReserveEndPond;
import org.folio.rest.jaxrs.model.Attend;
import org.folio.rest.jaxrs.model.Party;

public interface TakeInterface {

    default void update(){

    };
    default void setJob(Party partyGroup ){

    };
    default void removeAndFreed(Party partyGroup ){

    }
    default void updateJob(Party partyGroup ){

    }

    default void delJob(Party partyGroup ){

    }

    default void updatePartyAttendJob(Party partyGroup ){

    }
    default void setJob(Attend attend){

    }
    void timeTake();
    default void setIntCounter(String partyId, Handler<AsyncResult<Integer>> asyncResultHandler){

    }
    default Integer getIntCounter(String partyId){
        return 0;
    }
    default ReserveEndPond getReserveEndPond(){
        return null;
    }
    default Party getPartyById(String partyId){
        return null;
    }

    default void switchRules(){

    }
}
