package org.folio.rest.impl.other.delayjob.partydelay;

import io.vertx.core.Vertx;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class DelayThreadHeader {
    private static  ThreadPoolExecutor threadPool = new ThreadPoolExecutor(
            1,1,10, TimeUnit.SECONDS,
            new ArrayBlockingQueue<Runnable>(1),
            new ThreadPoolExecutor.DiscardOldestPolicy());
    public static PartyEndHandler initThread(Vertx vertx,String tenant)  {


        PartyEndHandler threadPoolParty = new PartyEndHandler(vertx,tenant);
        threadPool.execute(threadPoolParty);
        return threadPoolParty;
    }
}
