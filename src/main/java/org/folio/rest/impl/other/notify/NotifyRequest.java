package org.folio.rest.impl.other.notify;

public class NotifyRequest {
    /**
     * Match the activity Name of the template
     */
    private String activityName;
    /**
     * Match the user name of the template
     */
    private String name;
    /**
     * Match the phone of the template
     */
    private String phone;
    /**
     * Match the activity Start Time of the template
     */
    private String activityStartTime;
    /**
     * Match the activity End Time of the template
     */
    private String activityEndTime;

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getActivityStartTime() {
        return activityStartTime;
    }

    public void setActivityStartTime(String activityStartTime) {
        this.activityStartTime = activityStartTime;
    }

    public String getActivityEndTime() {
        return activityEndTime;
    }

    public void setActivityEndTime(String activityEndTime) {
        this.activityEndTime = activityEndTime;
    }
}
