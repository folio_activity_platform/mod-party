package org.folio.rest.impl.other.config;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

import javax.ws.rs.core.Response;
import java.util.Map;

/**
 * @author lee
 * @Classname ConfigService
 * @Description TODO
 * @Date 2020/6/29 18:24
 * @Created by lee
 */
@ProxyGen
public interface ConfigService {
    /**
     * 创建一个
     * @param vertx
     * @return
     */
    static ConfigService create(Vertx vertx){
      return new Config(vertx);
    }

    String get(String key);

    Future<Map<String, Object>> getAll();

    JsonObject initConfig();


    void getAll( Handler<AsyncResult<Map>> asyncResultHandler);
}
