package org.folio.rest.impl.other.delayjob;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lee
 * @Classname TenantsCollection
 * @Description TODO
 * @Date 2020/7/5 23:32
 * @Created by lee
 */
public class TenantsCollection {

    @JsonProperty("tenants")
    @JsonPropertyDescription("List of userdata items")
    @Valid
    @NotNull
    private List<TenantsPojo> tenants = new ArrayList<TenantsPojo>();


}
