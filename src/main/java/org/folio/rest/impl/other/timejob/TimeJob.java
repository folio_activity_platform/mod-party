package org.folio.rest.impl.other.timejob;

import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public abstract class TimeJob {
    private final static Logger logger = LoggerFactory.getLogger("modparty");
    public void job(){

    };
    public static void show(String message){
        logger.info(message);
    }
}
