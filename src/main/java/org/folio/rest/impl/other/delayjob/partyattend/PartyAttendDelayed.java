package org.folio.rest.impl.other.delayjob.partyattend;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * @author lee
 */
public class PartyAttendDelayed implements Delayed {
	
	 private String partyId;//活动编号
	 private Long expTime;//过期时间(活动签到结束时间）
	 private String tenantId;//资源归属的租户
	@Override
	public int compareTo(Delayed o) {
		// TODO Auto-generated method stub
		return this.expTime.compareTo(Long.valueOf(((PartyAttendDelayed)o).expTime));
	}

	@Override
	public long getDelay(TimeUnit unit) {
		// TODO Auto-generated method stub
		return this.expTime - System.currentTimeMillis();
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public Long getExpTime() {
		return expTime;
	}

	public void setExpTime(Long expTime) {
		this.expTime = expTime;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
     
}
