package org.folio.rest.impl.other.delayjob.partydelay;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;
public class PartyDelay implements Delayed {
    private String partyId;//活动编号
    private Long expTime;//过期时间
    private String deviceCount;//设备数量
    private String deviceId;//设备编号
    private String addressId;//场地编号
    private String addressCount;//场地数量
    private String tenantId;//资源归属的租户
    @Override
    public long getDelay(TimeUnit unit) {
        return this.expTime - System.currentTimeMillis();
    }

    @Override
    public int compareTo(Delayed o) {
        return this.expTime.compareTo(Long.valueOf(((PartyDelay)o).expTime));
    }

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    public Long getExpTime() {
        return expTime;
    }

    public void setExpTime(Long expTime) {
        this.expTime = expTime;
    }

    public String getDeviceCount() {
        return deviceCount;
    }

    public void setDeviceCount(String deviceCount) {
        this.deviceCount = deviceCount;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getAddressCount() {
        return addressCount;
    }

    public void setAddressCount(String addressCount) {
        this.addressCount = addressCount;
    }

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
}
