package org.folio.rest.impl.other.reserve_station;

import cn.hutool.core.date.DateUtil;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.folio.rest.impl.other.reserve_station.model.ReserveDelayed;


import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.DelayQueue;

/**
 * 主要负责待开始报名的活动,进入待命状态
 * 活动一旦开始报名,将会转交任务到待结束的队列中
 * 含有 添加任务 和 删除任务的方法暴露
 *
 */
public class ReserveStartPond implements Runnable  {

    private static volatile DelayQueue<ReserveDelayed> queueReserve = new DelayQueue<>();
    private final static Logger logger = LoggerFactory.getLogger("modparty");
    private static ConcurrentHashMap<String, Boolean> reserveStartStatusMap =
            new ConcurrentHashMap<String,Boolean>();
    private   ReserveEndPond reserveEndPond;
    private ReserveStation reserveStation;
    public ReserveStartPond( ) {

    }
    public void addReserveEndPond(ReserveEndPond reserveEndPond){
        this.reserveEndPond = reserveEndPond;

    }
    public void addReserveStation(ReserveStation reserveStation){
        this.reserveStation = reserveStation;
    }
    @Override
    public void run() {
        logger.info("已激活等待活动报名开始的队列，目前共有"+queueReserve.size()+"个活动准备开始报名");
        while (true){
            try {
                ReserveDelayed take = queueReserve.take();
                if (take !=null){
                    //活动报名开始
                    //将数据转移到另一个处理结束事件的队列
                    logger.info("活动：【"+take.getPartyId() +"】报名已开始，数据转移到待结束的队列中！");
                    String time = take.getEndTime();
                    long endTime = DateUtil.parseDateTime(time).getTime();
                    take.setExpTime(endTime);
                    reserveEndPond.addReserveDelayed(take);
                    reserveStartStatusMap.put(take.getPartyId(),true);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void add(ReserveDelayed reserveDelayed){
        Boolean status = reserveStartStatusMap.get(reserveDelayed.getPartyId());

        if (status == null){
            queueReserve.add(reserveDelayed);
            reserveStartStatusMap.put(reserveDelayed.getPartyId(),false);
            logger.info("活动:【"+reserveDelayed.getPartyId()+"】成功加入，等待报名队列");
        }

    }
    public static Boolean remove(final String partyId){
        Boolean status = reserveStartStatusMap.get(partyId);
        if (status == null || status){
            return null;
        }
        Optional<ReserveDelayed> reserve = queueReserve.stream().filter(a -> a.getPartyId().equals(partyId)).findAny();
        if (!reserve.isPresent()){
            return false;
        }
        queueReserve.remove(reserve.get());
        reserveStartStatusMap.remove(partyId);
        System.gc();
        return true;
    }

    public   void clearOutDate(){
        ConcurrentHashMap<String, Boolean> startStatusMap =
                new ConcurrentHashMap<String,Boolean>();
        reserveStartStatusMap.forEach((a,b)->{
            if (!b){
                startStatusMap.put(a, false);
            }else {

                reserveStation.clearPartyById(a);
            }
        });
        reserveStartStatusMap = startStatusMap;
    }

    /**
     *  The function is get a party reserved status, if result return is true , so
     *  party's start reserved , else party the reserved status is end or nulled
     * @param partyId
     * @return
     */
    public static Boolean getStatus(final String partyId){
        return reserveStartStatusMap.get(partyId);
    }


}
