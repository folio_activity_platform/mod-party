package org.folio.rest.impl.other.notify;

import io.vertx.core.Context;
import org.folio.cql2pgjson.exception.FieldException;
import org.folio.rest.impl.other.notify.impl.INotifyImpl;

import java.util.Map;

/**
 * @author lee
 */
public interface INotify {

    static INotifyImpl create(Map<String, String> okapiHeaders,
                              Context vertxContext, String accountType, String reserveChannel){
        return new INotifyImpl(okapiHeaders,vertxContext,accountType,reserveChannel);
    }

   void notifyCenter(Object object,Integer type) throws FieldException;
}
