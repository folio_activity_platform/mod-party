package org.folio.rest.impl.other;

import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.folio.cql2pgjson.CQL2PgJSON;
import org.folio.cql2pgjson.exception.FieldException;
import org.folio.rest.persist.cql.CQLWrapper;
import org.folio.rest.tools.messages.MessageConsts;
import org.folio.rest.tools.messages.Messages;

/**
 * @author lee
 * @Classname PartyUtil
 * @Description TODO
 * @Date 2020/6/19 13:53
 * @Created by lee
 */
public class PartyUtil {
    private static final Logger logger = LoggerFactory.getLogger("modparty");
    private static final Messages messages = Messages.getInstance();

    public static  CQLWrapper getCQL(String query, int limit, int offset,String NOTIFY_TABLE) throws FieldException {
        return new CQLWrapper(new CQL2PgJSON(NOTIFY_TABLE + ".jsonb"), query, limit, offset);
    }
    public static  CQLWrapper  CQLCreate(String query, int limit, int offset,String NOTIFY_TABLE)   {
       try{
           return new CQLWrapper(new CQL2PgJSON(NOTIFY_TABLE + ".jsonb"), query, limit, offset);
       }catch (Exception e){
           e.printStackTrace();
           return null;
       }
    }
    public static String internalErrorMsg(Exception e, String lang) {
        if (e != null) {
            logger.error(e.getMessage(), e);
        }
        String message = messages.getMessage(lang, MessageConsts.InternalServerError);
        if (e != null && e.getCause() != null && e.getCause().getClass().getSimpleName()
                .endsWith("CQLParseException")) {
            message = " CQL parse error " + e.getLocalizedMessage();
        }
        return message;
    }
}
