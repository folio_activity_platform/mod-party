package org.folio.rest.impl.other.timejob.impl;

import org.folio.rest.impl.other.take.TenantsTaskService;
import org.folio.rest.impl.other.timejob.TimeJob;

/**
 * @author lee
 * @Classname PartyTimeJob
 * @Description  定时在晚上12点处理 各自任务重置
 * @Date 2020/7/21 14:29
 * @Created by lee
 */
public class PartyTimeJob extends TimeJob {
    @Override
    public void job(){
        TenantsTaskService.timeTake( );
        super.show("每日定时任务扫描开始。。。");
    }
//    PartyAttendEndJob.job = 0 0 0 ? ? ?
//    PartyViewAmountUpdate.job = 0 0 0 ? ? ?
//    PartyReserveTimeJobs.job = 0 0 0 ? ? ?
//    PartyBlackListTimeJobImpl.job = 0 0 0 ? ? ?
}
