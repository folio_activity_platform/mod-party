package org.folio.rest.impl.other.client;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.ext.web.client.HttpResponse;
import org.folio.rest.jaxrs.model.User;
import org.folio.rest.jaxrs.model.UserdataCollection;

import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author lee
 * @Classname OkapiUserModulesClient
 * @Description TODO
 * @Date 2020/6/23 13:37
 * @Created by lee
 */
public interface OkapiUserModulesClient {

    Future<UserdataCollection> getUserGroup(String name);

    void updateUser(User user, Handler<AsyncResult< cn.hutool.http.HttpResponse  >> future);

    void getUserGroupByBarcode(List<String> barcode, Handler<AsyncResult<UserdataCollection>> asyncResultHandler);

    Future<UserdataCollection> fetchUser(String query,Integer offset,Integer limit);
}
