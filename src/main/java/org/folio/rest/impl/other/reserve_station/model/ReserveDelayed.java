package org.folio.rest.impl.other.reserve_station.model;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class ReserveDelayed  implements Delayed {

    private String partyId;//活动编号
    private Long expTime;//过期时间(活动报名开始时间）
    private String tenantId;//活动归属的租户
    private Integer reserveAmount;//报名的总名额
    private String endTime;//活动报名结束时间

    @Override
    public int compareTo(Delayed o) {
        // TODO Auto-generated method stub
        return this.expTime.compareTo(Long.valueOf(((ReserveDelayed)o).expTime));
    }

    @Override
    public long getDelay(TimeUnit unit) {
        // TODO Auto-generated method stub
        return this.expTime - System.currentTimeMillis();
    }

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    public Long getExpTime() {
        return expTime;
    }

    public void setExpTime(Long expTime) {
        this.expTime = expTime;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public Integer getReserveAmount() {
        return reserveAmount;
    }

    public void setReserveAmount(Integer reserveAmount) {
        this.reserveAmount = reserveAmount;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
