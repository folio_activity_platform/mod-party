package org.folio.rest.impl.other.delayjob.partyattend;

import java.util.List;

import org.folio.rest.jaxrs.model.ReaderReserveGroup;

public class ReserveSearchResult {
 
 
	private String reserveId;
	
	private List<ReaderReserveGroup> readerReserveGroup;
	
	private String attendId;
	
	private String attendState;
	
	private String propertyName;
	
	private String partyName;

	private String partyStartDate;

	private String reserveDate;

	public String getReserveId() {
		return reserveId;
	}

	public void setReserveId(String reserveId) {
		this.reserveId = reserveId;
	}

	public List<ReaderReserveGroup> getReaderReserveGroup() {
		return readerReserveGroup;
	}

	public void setReaderReserveGroup(List<ReaderReserveGroup> readerReserveGroup) {
		this.readerReserveGroup = readerReserveGroup;
	}

	public String getAttendId() {
		return attendId;
	}

	public void setAttendId(String attendId) {
		this.attendId = attendId;
	}

	public String getAttendState() {
		return attendState;
	}

	public void setAttendState(String attendState) {
		this.attendState = attendState;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public String getPartyStartDate() {
		return partyStartDate;
	}

	public void setPartyStartDate(String partyStartDate) {
		this.partyStartDate = partyStartDate;
	}

	public String getReserveDate() {
		return reserveDate;
	}

	public void setReserveDate(String reserveDate) {
		this.reserveDate = reserveDate;
	}
}
