package org.folio.rest.impl.other;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import org.folio.rest.RestVerticle;
import org.folio.rest.jaxrs.model.Metadata;
import org.folio.rest.jaxrs.model.Operator;
import org.folio.rest.persist.PostgresClient;

import javax.ws.rs.core.Response;
import java.util.Map;
import java.util.UUID;

/**
 * @author lee
 * @Classname PartyLogSave
 * @Description TODO
 * @Date 2020/6/26 21:41
 * @Created by lee
 */
public class PartyLogSave {
//     "id": "xxxxxx-xxxxxx-xxxxx-xxxxxxx",
//             *                           "ip": "127.0.0.1",
//             *                           "type": 2,
//             *                           "userId": "15fa3eda-f495-496c-b21e-4f281b38a3ef",
//             *                           "describe": "新增名称为热干面的分类",
//             *                           "username": "koss",
   public static void saveLog(String describe, Integer type, Metadata metadata, Map<String, String> okapiHeaders, Handler<AsyncResult<Response>> asyncResultHandler, PostgresClient postgresClient){
       String name = okapiHeaders.get(RestVerticle.OKAPI_HEADER_TENANT);
       String userId = okapiHeaders.get(RestVerticle.OKAPI_USERID_HEADER);
       String ip = okapiHeaders.get("x-okapi-request-ip");
       String id = UUID.randomUUID().toString();
       org.folio.rest.jaxrs.model.PartyLog partyLog = new org.folio.rest.jaxrs.model.PartyLog();
       partyLog.setUsername(name);
       partyLog.setUserId(userId);
       partyLog.setIp(ip);
       partyLog.setId(id);
       partyLog.setDescribe(describe);
       partyLog.setType(type);
       partyLog.setMetadata(metadata);
       partyLog.setIsDel(0);
       postgresClient.save( "party_log",partyLog.getId(),partyLog,null );
       postgresClient.closeClient(a->{});

   }
    public static void saveLog(String describe, Integer type, Metadata metadata, Operator operator, Map<String, String> okapiHeaders, Handler<AsyncResult<Response>> asyncResultHandler, PostgresClient postgresClient){
       if (operator !=null){
           String name = operator.getFirstName()+" "+operator.getLastName();
           String userId = okapiHeaders.get(operator.getId());
           String ip = okapiHeaders.get("x-okapi-request-ip");
           String id = UUID.randomUUID().toString();
           org.folio.rest.jaxrs.model.PartyLog partyLog = new org.folio.rest.jaxrs.model.PartyLog();
           partyLog.setUsername(name);
           partyLog.setUserId(userId);
           partyLog.setIp(ip);
           partyLog.setId(id);
           partyLog.setDescribe(describe);
           partyLog.setType(type);
           partyLog.setMetadata(metadata);
           partyLog.setIsDel(0);
           postgresClient.save( "party_log",partyLog.getId(),partyLog,null );
       }

        postgresClient.closeClient(a->{});

    }
    public static void saveLog(String describe, Integer type, Metadata metadata ,   PostgresClient postgresClient){


            String ip =  "sys";
            String id = UUID.randomUUID().toString();
            org.folio.rest.jaxrs.model.PartyLog partyLog = new org.folio.rest.jaxrs.model.PartyLog();
            partyLog.setUsername("sys");
            partyLog.setUserId("sys");
            partyLog.setIp(ip);
            partyLog.setId(id);
            partyLog.setDescribe(describe);
            partyLog.setType(type);
            partyLog.setMetadata(metadata);
            partyLog.setIsDel(0);
            postgresClient.save( "party_log",partyLog.getId(),partyLog,null );


    }
}
