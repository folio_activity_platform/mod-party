package org.folio.rest.impl.other.delayjob;

/**
 * @author lee
 * @Classname TenantsPojo
 * @Description TODO
 * @Date 2020/7/5 23:27
 * @Created by lee
 */
public class TenantsPojo {
    private String name;
    private String description;
    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
