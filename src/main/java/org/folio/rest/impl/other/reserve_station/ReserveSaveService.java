package org.folio.rest.impl.other.reserve_station;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.ext.sql.ResultSet;
import org.folio.rest.jaxrs.model.Reserve;
import org.folio.rest.persist.PgUtil;
import org.folio.rest.persist.PostgresClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.stream.Collectors;

public class ReserveSaveService {
    private static final String NOTIFY_TABLE = "reserve";
    private ReserveQueueService serve;
    private Vertx vertx;
    private String tenant;

    public ReserveSaveService(ReserveQueueService serve, Vertx vertx, String tenant) {

        this.serve = serve;
        this.vertx = vertx;
        this.tenant = tenant;

    }

    public void save(Handler<AsyncResult<Boolean>> asyncResultHandler){
        CopyOnWriteArraySet<Reserve> basket = serve.getBasket();
        List<Reserve> array = basket.stream().collect(Collectors.toList());
        PostgresClient pgUtil = PostgresClient.getInstance(vertx, tenant);
        pgUtil.saveBatch(NOTIFY_TABLE,array,reply->{
            if (reply.succeeded()){

                asyncResultHandler.handle(Future.succeededFuture(true));
            }else{
                asyncResultHandler.handle(Future.failedFuture(reply.cause()));
            }
        });
    }
}
