package org.folio.rest.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import org.junit.Test;

import java.util.Arrays;
import java.util.Date;

public class TimeTest {

    @Test
    public void test(){
        String timeStart = "2020-09-24 16:10:00";
        String timeEnd = "2020-09-24 16:55:00";
        Date date = new Date();
        DateTime dateEnd = DateUtil.parseDateTime(timeEnd);
        System.out.println(date.before(dateEnd.toJdkDate()));

        Arrays.asList(1,2,3,4,5,6,7,9,10).forEach(a->{
            if (a == 6){
                System.out.println(a);
                return;
            }
        });
        System.out.println("end");
    }
}
