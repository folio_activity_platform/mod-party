package org.folio.rest.impl;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;

import org.folio.rest.jaxrs.model.Appraisal;
import org.folio.rest.jaxrs.model.PartyGroup;
import org.folio.rest.persist.PostgresClient;
import org.junit.Test;

import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONUtil;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.*;
import java.util.function.Function;

import static org.apache.http.HttpStatus.*;

/**
 * @author lee
 * @Classname VertxClientTest
 * @Description TODO
 * @Date 2020/6/29 14:41
 * @Created by lee
 */
public class VertxClientTest  {

    @Test
    public void start() throws IOException   {
        String json = "{\"id\": \"a167f4d0-74ab-49d8-8d3c-506e85d05186\", \"score\": 5, \"state\": \"1\", \"idList\": [], \"partyId\": \"c51051bc-37b6-4dcd-b7ec-c52164383636\", \"metadata\": {\"createdDate\": \"2020-09-22T02:29:57.951\", \"updatedDate\": \"2020-09-22T02:29:57.951+0000\", \"createdByUserId\": \"15fa3eda-f495-496c-b21e-4f281b38a3ef\", \"updatedByUserId\": \"15fa3eda-f495-496c-b21e-4f281b38a3ef\"}, \"appraisal\": \"这个活动真的非常棒,希望以后还能继续参与!\", \"partyName\": \"好饿的毛毛虫\", \"reserveId\": \"0d714de7-2969-4116-a75b-efde1aa2d55b\", \"createDate\": \"2020-09-22 10:29:57\", \"propertyName\": \"少儿1\", \"partyStartDate\": \"2020-08-05 09:00:00\", \"readerReserveGroup\": [{\"name\": \"Narciso Koss\", \"barcode\": \"971460036650480\", \"mobilePhone\": \"18027268831\"}]}";
        Appraisal app = JSONUtil.toBean(json, Appraisal.class);
        List<String> stringList = new ArrayList<>();
        for (int i = 0; i < 40; i++) {
            String id = UUID.randomUUID().toString();
            System.out.println(id);
            stringList.add(id);
        }
        for (String s:
             stringList) {
            app.setId(s);
            System.out.println(JSONUtil.toJsonStr(app));
        }
    }

 
}
