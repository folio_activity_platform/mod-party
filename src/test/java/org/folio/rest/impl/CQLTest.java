package org.folio.rest.impl;

import cn.hutool.json.JSONUtil;
import org.folio.cql2pgjson.CQL2PgJSON;
import org.folio.cql2pgjson.exception.FieldException;
import org.folio.rest.jaxrs.model.PunishmentRecordGroup;
import org.folio.rest.jaxrs.model.ReaderCreditRecord;
import org.folio.rest.persist.cql.CQLWrapper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author lee
 * @Classname org.folio.rest.impl.CQLTest
 * @Description TODO
 * @Date 2020/6/21 22:10
 * @Created by lee
 */
public class CQLTest {
    public static CQLWrapper getCQL(String query, int limit, int offset, String NOTIFY_TABLE) throws FieldException {
        return new CQLWrapper(new CQL2PgJSON(NOTIFY_TABLE + ".jsonb"), query, limit, offset);
    }

    private static final String NOTIFY_TABLE = "party";

    public static void main(String[] args) throws FieldException {
//        String query = "(isDel = 0 and partyId == *39d34a1f-bd0c-4790-af59-1f3a74f6c5f0*)";
//        CQLWrapper where = PartyUtil.CQLCreate(query, -1, -1, "r");
//        String tenant = "diku";
//        String sql = "SELECT %s FROM " + tenant + "_mod_party.reserve r LEFT JOIN " + tenant + "_mod_party.attend a ON r.jsonb ->> 'partyId' = a.jsonb ->> 'partyId'";
//        String responseParams = "r.jsonb ->> 'id' AS reserve_id,r.jsonb ->> 'reserveDate' AS reserve_date, r.jsonb ->> 'partyName' AS party_name ," +
//                "                 r.jsonb ->> 'partyStartDate' AS party_start_date , " +
//                " r.jsonb ->> 'reserveDate' AS reserve_date , "
//                + " r.jsonb -> 'readerReserveGroup' as reader_reserve_group ," +
//                " a.jsonb ->> 'id' as attend_id ," +
//                "  case when a.jsonb ->> 'attendState' is null then '5' else  a.jsonb ->> 'attendState' end attend_state ";
//        String searchSQL = String.format(sql, responseParams) +where.toString();
//        CQLWrapper searchSQL = PartyUtil.CQLCreate("punishmentRecordGroup == *\"createDate\"*: *\"2020-10-30\"* sortby year/sort.descending  metadata.updatedDate/sort.descending ", 10, 0, DBName.NOTIFY_TABLE_PARTY_RULES);
//        System.out.println(searchSQL);
        ReaderCreditRecord readerCreditRecord = new ReaderCreditRecord();
        List<PunishmentRecordGroup> pum = new ArrayList<>();
        PunishmentRecordGroup p = new PunishmentRecordGroup();
        p.setStatus("1");
        pum.add(p);
        readerCreditRecord.setPunishmentRecordGroup(pum);

        System.out.println(JSONUtil.toJsonPrettyStr(readerCreditRecord));
    }
    public static List<String> getDays(String startTime, String endTime) {

        // 返回的日期集合
        List<String> days = new ArrayList<String>();

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date start = dateFormat.parse(startTime);
            Date end = dateFormat.parse(endTime);

            Calendar tempStart = Calendar.getInstance();
            tempStart.setTime(start);

            Calendar tempEnd = Calendar.getInstance();
            tempEnd.setTime(end);
            tempEnd.add(Calendar.DATE, +1);// 日期加1(包含结束)
            while (tempStart.before(tempEnd)) {
                days.add(dateFormat.format(tempStart.getTime()));
                tempStart.add(Calendar.DAY_OF_YEAR, 1);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return days;
    }
    private static void sql(){
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(" select f.userName as reader_name, ");
        stringBuffer.append(" f.readerAccount as reader_number, ");
        stringBuffer.append(" sum(f.count) as registrations_number, ");
        stringBuffer.append(" sum(f.checkin) as check_in_number, ");
//    stringBuffer.append(" sum(f.absence) as reader_absence_count, ");
        stringBuffer.append(" sum(f.leave) as leave, ");
        stringBuffer.append(" CASE WHEN sum(f.count) = 0 THEN '0.00%' ELSE concat(round(sum(f.checkin)\\:\\:numeric/sum(f.count),2)*100,'%') END check_in_rate, ");
        stringBuffer.append(" CASE WHEN sum(f.count) = 0 THEN '0.00%' ELSE concat(round((sum(f.count)-sum(f.checkin))\\:\\:numeric/sum(f.count),2)*100,'%') END absenteeism_rate, ");
        stringBuffer.append(" CASE WHEN sum(f.count) = 0 THEN '0.00%' ELSE concat(round(sum(f.leave)\\:\\:numeric/sum(f.count),2)*100,'%') END leave_rate ");
        stringBuffer.append(" from (select b.*,count(b.userName) as count, ");
        stringBuffer.append("  COUNT(case when b.attendanceState = '1' then b.userName end) as checkin, ");
        stringBuffer.append(" COUNT(case when b.attendanceState = '0' then b.userName end) as absence, ");
        stringBuffer.append(" COUNT(case when b.attendanceState = '3' then b.userName end) as leave ");
        stringBuffer.append(" from (select a.jsonb ->> 'readerName'      as userName, ");
        stringBuffer.append("  c.jsonb ->> 'activityName'    as activityName, ");
        stringBuffer.append("  c.jsonb ->> 'attendanceState' as attendanceState, ");
        stringBuffer.append(" a.jsonb ->> 'readerAccount'   as readerAccount ");
        stringBuffer.append(" from diku_mod_party.reserve as a ");
        stringBuffer.append(" left join  diku_mod_party.append as c on a.jsonb ->> 'id' = c.registration_id ");
        stringBuffer.append(" where 1 = 1 and a.isdel = 1  ");
        stringBuffer.append(" ) as b ");
        stringBuffer.append("  group by b.userName, b.activityName, b.attendanceState, b.readerAccount  ");
        stringBuffer.append("      ) as f ");
        stringBuffer.append(" group by f.userName, f.readerAccount  ");
        System.out.println(stringBuffer.toString());
    }

    private static void partyDataSQL(){
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(" SELECT f.activityName AS activity_name, f.count AS registration_count, f.activityId AS id, ");
        stringBuffer.append(" f.activityStartDate AS activity_start_date, f.checkin, f.absence, f.leave, ");
        stringBuffer.append(" CASE WHEN f.count = 0 THEN '0.00%' ELSE concat(round(sum(f.checkin)\\:\\:numeric / sum(f.count), 2) * 100, '%') END checkin_percent, ");
        stringBuffer.append(" CASE WHEN f.count = 0 THEN '0.00%' ELSE concat(round(sum(f.absence)\\:\\:numeric / sum(f.count), 2) * 100, '%') END absence_percent, ");
        stringBuffer.append(" CASE WHEN f.count = 0 THEN '0.00%' ELSE concat(round(sum(f.leave)\\:\\:numeric / sum(f.count), 2) * 100, '%') END leave_percent, ");
        stringBuffer.append(" COALESCE(s.count, 0) AS view_count from(SELECT d.activityId, d.quota, d.activityName, d.activityStartDate, ");
        stringBuffer.append(" count(d.registrationId) AS count, COUNT(case WHEN d.act_check_in = '1' THEN d.activityname end) AS checkin, ");
        stringBuffer.append(" COUNT(case WHEN d.act_check_in = '0' THEN d.activityname end) AS absence, ");
        stringBuffer.append(" COUNT(case WHEN d.act_check_in = '3' THEN d.activityname end) AS leave ");
        stringBuffer.append(" FROM (SELECT a.jsonb ->> 'id' AS activityId, a.jsonb ->> 'activityName' AS activityName, ");
        stringBuffer.append(" a.jsonb ->> 'quota' AS quota, s.jsonb ->> 'attendanceState' AS act_check_in, a.jsonb ->> 'category', ");
        stringBuffer.append(" a.jsonb ->> 'activityStartDate' AS activityStartDate, c.id AS registrationId FROM ");
        stringBuffer.append( "diku_mod_party.party AS a LEFT JOIN ");
        stringBuffer.append( "diku_mod_party.reserve AS c ON a.jsonb ->> 'id' = c.jsonb ->> 'activityId' LEFT JOIN ");
        stringBuffer.append( "diku_mod_party.attend AS s ON c.jsonb ->> 'id' = s.registration_id WHERE 1 = 1 AND ");
        stringBuffer.append(" a.isdel = 1 AND (c.isdel = 1 OR c.isdel is null)");
//        if (!StrUtil.isBlankOrUndefined(jsonB.getCategory())){
//            stringBuffer.append(" and (a.jsonb ->> 'category') like " + "'%" + jsonB.getCategory() + "%'");
//        }
//        if (!StrUtil.isBlankOrUndefined(jsonB.getActivityName())){
//            stringBuffer.append(" and (a.jsonb ->> 'activityName') like " + "'%" + jsonB.getActivityName() + "%'");
//        }
//        if (ObjectUtil.isNotNull(param.getActivityStartDateTime()) && ObjectUtil.isNotNull(param.getActivityEndDateTime()) ) {
//            stringBuffer.append(" and a.activitystartdatetime >= to_timestamp('"+param.getActivityStartDateTime()+"','yyyy-MM-dd hh24:mi:ss') ");
//            stringBuffer.append(" and a.activityenddatetime <= to_timestamp('"+param.getActivityEndDateTime()+"','yyyy-MM-dd hh24:mi:ss') ");
//        }
//        if (ObjectUtil.isNotNull(param.getActivityStartDateTime()) && ObjectUtil.isNull(param.getActivityEndDateTime())) {
//            stringBuffer.append(" and a.activitystartdatetime >= to_timestamp('"+param.getActivityStartDateTime()+"','yyyy-MM-dd hh24:mi:ss') ");
//
//        }
//        if (ObjectUtil.isNull(param.getActivityStartDateTime()) && ObjectUtil.isNotNull(param.getActivityEndDateTime())) {
//            stringBuffer.append(" and a.activityenddatetime <= to_timestamp('"+param.getActivityEndDateTime()+"','yyyy-MM-dd hh24:mi:ss') ");
//        }
//        stringBuffer.append("  ) AS d GROUP BY d.activityId, d.quota,d.activityName, d.activityStartDate) AS f LEFT JOIN  ");
//        stringBuffer.append(systemVariables.OKAPI_TENANT+".db_activity_view_statistics AS s ON f.activityId = s.activityid WHERE 1 = 1 ");
//        stringBuffer.append(" GROUP BY f.activityId, f.quota, f.checkin, f.absence, f.leave, f.activityStartDate, f.count, f.activityName, s.count ");
//
//        if (ObjectUtil.isNotEmpty(pageUtil))
//        {
//            stringBuffer.append(" limit " + pageUtil.getSize() + " offset " + pageUtil.getNextPage() + " ;");
//
//        }else
//        {
//            stringBuffer.append(" ; ");
//        }

        System.out.println(stringBuffer.toString());
    }
}
