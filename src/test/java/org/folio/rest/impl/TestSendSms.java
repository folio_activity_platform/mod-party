package org.folio.rest.impl;

import cn.hutool.core.util.StrUtil;
import io.vertx.core.Future;
import org.folio.rest.jaxrs.model.UrgentNotify;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author lee
 * @Classname TestSendSms
 * @Description TODO
 * @Date 2020/8/12 11:37
 * @Created by lee
 */
public class TestSendSms {

    @Test
    public void test1(){
        UrgentNotify entity = new UrgentNotify();
        entity.setMessage("");
       // entity.setBarcodeGroup(Arrays.asList("21312312"));
        UrgentNotify entity2 = new UrgentNotify();
        entity2.setMessage("12312312312");
        //entity2.setBarcodeGroup(Arrays.asList("21312312"));
        Map<Boolean, List<UrgentNotify>> map = Arrays.asList(entity, entity2).stream().collect(Collectors.partitioningBy(a -> StrUtil.isBlankOrUndefined(a.getMessage())));
        System.out.println(map.toString());
    }
    @Test
    public void test2(){
       String message = "test";
       List<String> barcode = new ArrayList<>();
       barcode.add("123123323232");
       barcode.add("454354534534");
        barcode.add("sssss23232");
        barcode.add("45dddddd34534");
         barcode.parallelStream().forEach(code->{
             String result = testFuture(code, message).result();
             System.out.println(result);
         });
        System.out.println("ok");
    }

    private Future<String> testFuture(String barcode,String message){
        Future<String> future = Future.future();
        try{
            Thread.sleep(1500);
            future.complete("success");
            return future;
        }catch (Exception e){

        }
        future.tryComplete("err");
        return future ;
    }
}
