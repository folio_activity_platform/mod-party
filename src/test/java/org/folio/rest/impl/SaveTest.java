package org.folio.rest.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import org.folio.rest.impl.other.PartyUtil;
import org.folio.rest.impl.util.DBName;
import org.folio.rest.persist.cql.CQLWrapper;
import org.junit.Test;

import java.util.Date;

public class SaveTest {
    @Test
    public void test1(){
        String query = "( status == 1 )";
        CQLWrapper cql = PartyUtil.CQLCreate(query, -1, -1, DBName.NOTIFY_TABLE_READER_CREDIT);
        System.out.println(cql);
        DateTime date = DateUtil.offsetDay(new Date(), -1);
        System.out.println(date.toDateStr());
        System.out.println(  DateUtil.parse("2020-10-31").isBefore(new Date()) );
    }
}
